/* 
 * File:   RevVBOQuadVT.h
 * Author: Revers
 *
 * Created on 3 czerwiec 2012, 13:03
 */

#ifndef REVVBOQUADVT_H
#define	REVVBOQUADVT_H

namespace rev {

    class VBOQuadVT {
        unsigned int vaoHandle[1];
        unsigned int handle[2];

        float xMin;
        float xMax;
        float yMin;
        float yMax;

    public:
        VBOQuadVT(float length = 1.0f);
        VBOQuadVT(float xMin, float xMax, float yMin, float yMax);

        ~VBOQuadVT();

        void render();

        void create(float xMin, float xMax, float yMin, float yMax);

    private:
        bool generate();

        void initVAO(int contextIndex);
    };

}
#endif	/* REVVBOQUADVT_H */

