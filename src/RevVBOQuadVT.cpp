#include <GL/glew.h>
#include <GL/gl.h>
#include "RevVBOQuadVT.h"
#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevAssert.h>

using namespace rev;

VBOQuadVT::VBOQuadVT(float length) {
 //   create(-length, length, -length, length);
}

VBOQuadVT::VBOQuadVT(float xMin, float xMax, float yMin, float yMax) {
    //create(xMin, xMax, yMin, yMax);
}

void VBOQuadVT::create(
        float xMin,
        float xMax,
        float yMin,
        float yMax) {
    this->xMin = xMin;
    this->xMax = xMax;
    this->yMin = yMin;
    this->yMax = yMax;

    vaoHandle[0] = 0;
    glGenBuffers(2, handle);

    bool succ = generate();
    revAssert(succ);

}

VBOQuadVT::~VBOQuadVT() {
    glDeleteBuffers(2, handle);
}

void VBOQuadVT::initVAO(int contextIndex) {
    if (vaoHandle[contextIndex] == 0) {
        glGenVertexArrays(1, &vaoHandle[contextIndex]);
    }
    glBindVertexArray(vaoHandle[contextIndex]);

    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glVertexAttribPointer((GLuint) 0, 3, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(0); // Vertex position
    //-------------------------------------------------------------
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glVertexAttribPointer((GLuint) 1, 2, GL_FLOAT, GL_FALSE, 0,
            ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(1); // Texture coordinates
    //-------------------------------------------------------------
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

bool VBOQuadVT::generate() {
    GLfloat verts[] = {
            xMin, yMin, 0.0f,
            xMax, yMin, 0.0f,
            xMax, yMax, 0.0f,
            xMin, yMin, 0.0f,
            xMax, yMax, 0.0f,
            xMin, yMax, 0.0f
    };
    GLfloat tc[] = {
            0.0f, 0.0f,
            1.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 1.0f,
            0.0f, 1.0f
    };

    // just in case I forgot to unbind some VAO:
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glBufferData(GL_ARRAY_BUFFER, 6 * 3 * sizeof(float), verts, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glBufferData(GL_ARRAY_BUFFER, 6 * 2 * sizeof(float), tc, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    initVAO(0);

    glAssert;
    return glGetError() == GL_NO_ERROR;
}

void VBOQuadVT::render() {
    glBindVertexArray(vaoHandle[0]);

    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}