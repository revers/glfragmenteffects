/* 
 * File:   RenderTargetDefines.h
 * Author: Revers
 *
 * Created on 14 czerwiec 2012, 20:40
 */

#ifndef RENDERTARGETDEFINES_H
#define	RENDERTARGETDEFINES_H

#define REV_MIN_FILTER GL_NEAREST
#define REV_MAG_FILTER GL_NEAREST
#define REV_WRAP_S GL_CLAMP_TO_EDGE
#define REV_WRAP_T GL_CLAMP_TO_EDGE

#endif	/* RENDERTARGETDEFINES_H */

