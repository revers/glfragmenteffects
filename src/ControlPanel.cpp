/* 
 * File:   ControlPanel.cpp
 * Author: Revers
 * 
 * Created on 26 kwiecień 2012, 20:33
 */
#include <GL/glew.h>

#include <iostream>
#include <boost/algorithm/string.hpp>

#include "ControlPanel.h"
#include "LoggingDefines.h"
#include "AllEffects.h"
#include "RenderTargetDefines.h"
#include <rev/gl/RevGLUtil.h>

#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevStringUtil.h>

using namespace rev;
using namespace log4cplus;
using namespace boost::filesystem;
using namespace std;

#define X_MIN -0.4f
#define X_MAX 1.0f
#define Y_MIN -1.0f
#define Y_MAX 1.0f
#define EFFECT_TOOLBAR_WIDTH 340
#define EFFECT_TOOLBAR_HEIGHT 345
#define CONTROL_TOOLBAR_WIDTH EFFECT_TOOLBAR_WIDTH
#define CONTROL_TOOLBAR_HEIGHT 240
#define CONTROL_TOOLBAR_POS_X 10
#define CONTROL_TOOLBAR_POS_Y 5
#define EFFECT_TOOLBAR_POS_X CONTROL_TOOLBAR_POS_X
#define EFFECT_TOOLBAR_POS_Y 253

#define AA_SHADER_FILENAME "shaders/FXAA.glsl"

Logger ControlPanel::logger = Logger::getInstance("ControlPanel");

inline std::string pathToString(const boost::filesystem::path& p) {
    const wchar_t* wFilename = p.c_str();
    return StringUtil::fromWideString(wFilename);
}

ControlPanel::ControlPanel() {
    texture.setInitialInternalFormat(GL_RGB8);
    texture.setInitialSampler(GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT);

    lmbPressed = false;
    currentEffect = NULL;
    currentEffectStr = NULL;
    selectedBackgroundIndex = 0;
    effectBar = NULL;
    mouseOver = false;
    widthOffset = 0;
    lastMouseWheelPos = 0;
    totalTime = 0.0f;
    speed = 1.0f;
    paused = true;

    aaOn = true;
    aaLevel = 1;
    aaScale = 0.006f;

    fxaaActive = true;
    fxaaSpanMax = 8.0f;
    fxaaReduceMul = 1.0f / 8.0f;
    fxaaVxOffset = 0.0f;
    fxaaSubpixShift = 1.0f / 4.0f;
}

ControlPanel::~ControlPanel() {
    for (EffectMap::iterator it = effectMap.begin();
            it != effectMap.end(); ++it) {
        delete it->second;
    }
    TwTerminate();
}

bool ControlPanel::init() {
    if (!TwInit(TW_OPENGL_CORE, NULL)) {
        LOG_ERROR(logger, TwGetLastError());
        return false;
    }

    glAlwaysAssert;

    quadVBO.create(X_MIN, X_MAX, Y_MIN, Y_MAX);
        //LOG_ERROR(logger, "quadVBO.create(X_MIN, X_MAX)) FAILED!!");
        //return false;
    //}

    glAlwaysAssert;

    secondQuadVBO.create(-1, 1, -1, 1);// {
        //LOG_ERROR(logger, "secondQuadVBO.create() FAILED!!");
        //return false;
    //}

    glAlwaysAssert;

    if (!initFXAAShader()) {
        LOG_ERROR(logger, "initAAShader() FAILED!!");
        return false;
    }

    glAlwaysAssert;

    TwDefine(" TW_HELP visible=false ");

    controlBar = TwNewBar("Control");
    TwDefine(" Control color='20 20 20' ");
    TwDefine(" Control valueswidth=165 fontSize=3 ");



    effectBar = TwNewBar("Effects");
    TwDefine(" Effects color='20 20 20' ");
    TwDefine(" Effects valueswidth=165 fontSize=3 ");

    //TwAddSeparator(controlBar, "__contSep1", NULL);
    if (!loadBackgroundImages()) {
        LOG_ERROR(logger, "ControlPanel::loadBackgroundImages() FAILED!!");
        return false;
    }

    glAlwaysAssert;

    TwAddVarRO(controlBar, "__AA", TW_TYPE_BOOLCPP, &(aaOn), "label='AA on/off' ");

    TwAddVarCB(controlBar, "__AALevel", TW_TYPE_INT32, setAALevelCallback, getAALevelCallback, this,
            "label='AA Level' min=1 max=10 step=1 ");

    TwAddVarCB(controlBar, "__AAScale", TW_TYPE_FLOAT, setAAScaleCallback, getAAScaleCallback, this,
            "label='AA Scale' min=0.001 max=25.0 step=0.001 ");
    
    TwAddVarRW(controlBar, "FXAA_Active", TW_TYPE_BOOLCPP, &fxaaActive, "label='FXAA Active'");


    TwCopyCDStringToClientFunc(copyCDStringToClient);
    TwAddVarRO(controlBar, "__currEffect", TW_TYPE_CDSTRING, &currentEffectStr,
            " label='Current Effect:' ");

    TwAddVarRO(controlBar, "__TIME", TW_TYPE_FLOAT, &totalTime,
            " label='Time' ");

    TwAddVarRW(controlBar, "__Pause", TW_TYPE_BOOLCPP, &(paused), "label='Pause' ");

    TwAddVarRW(controlBar, "__Speed", TW_TYPE_FLOAT, &speed, " label='Speed' min=0.1 max=100.0 step=0.1 ");

    TwAddButton(controlBar, "__ResetTime", resetTimeCallback,
            this, "label='Reset Time' ");

    TwAddButton(controlBar, "__ResetCurr", resetButtonCallback,
            this, "label='Reset Effect' ");

    if (!createFXAAPanel()) {
        LOG_ERROR(logger, "createFXAAPanel() FAILED!!");
        return false;
    }

    if (!addEffects()) {
        LOG_ERROR(logger, "ControlPanel::addEffects() FAILED!!");
        return false;
    }

    struct {
        int x;
        int y;
    } controlPos = {CONTROL_TOOLBAR_POS_X, CONTROL_TOOLBAR_POS_Y};
    TwSetParam(controlBar, NULL, "position", TW_PARAM_INT32, 2, &controlPos);

    struct {
        int x;
        int y;
    } filterPos = {EFFECT_TOOLBAR_POS_X, EFFECT_TOOLBAR_POS_Y};
    TwSetParam(effectBar, NULL, "position", TW_PARAM_INT32, 2, &filterPos);

    struct {
        int x, y;
    } controlSize = {CONTROL_TOOLBAR_WIDTH, CONTROL_TOOLBAR_HEIGHT};
    TwSetParam(controlBar, NULL, "size", TW_PARAM_INT32, 2, &controlSize);

    struct {
        int x, y;
    } filterSize = {EFFECT_TOOLBAR_WIDTH, EFFECT_TOOLBAR_HEIGHT};
    TwSetParam(effectBar, NULL, "size", TW_PARAM_INT32, 2, &filterSize);

    glAlwaysAssert;

    LOG_TRACE(logger, "init() SUCCESS :)");

    return true;
}

#define FXAA_PANEL_NAME "FXAA"

bool ControlPanel::createFXAAPanel() {
//    TwAddVarRW(effectBar, "Active", TW_TYPE_BOOLCPP, &fxaaActive, "group='" FXAA_PANEL_NAME "'");

    TwAddVarCB(effectBar, "fxaaSpanMax", TW_TYPE_FLOAT, setFxaaSpanMaxCallback, getFxaaSpanMaxCallback, this,
            "label='Span Max' min=0.0 max=25.0 step=0.5 group='" FXAA_PANEL_NAME "'");

    TwAddVarCB(effectBar, "fxaaReduceMul", TW_TYPE_FLOAT, setFxaaReduceMulCallback, getFxaaReduceMulCallback, this,
            "label='Reduce Mul' min=0.0 max=10.0 step=0.005 group='" FXAA_PANEL_NAME "'");

    TwAddVarCB(effectBar, "fxaaSubpixShift", TW_TYPE_FLOAT, setFxaaSubpixShiftCallback, getFxaaSubpixShiftCallback, this,
            "label='Subpix Shift' min=0.0 max=10.0 step=0.01 group='" FXAA_PANEL_NAME "'");

    return true;
}

bool ControlPanel::initFXAAShader() {

    if (!aaProgram.compileShaderGLSLFile(AA_SHADER_FILENAME)) {
        LOG_ERROR(logger, "Compilation of file " AA_SHADER_FILENAME " FAILED!!");
        return false;
    }

    glBindAttribLocation(aaProgram.getHandle(), 0, "VertexPosition");
    glBindAttribLocation(aaProgram.getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(aaProgram.getHandle(), 0, "FragColor");

    if (!aaProgram.link()) {
        LOG_ERROR(logger, "Linking program FAILED!!");
        return false;
    }

    aaProgram.use();
    aaProgram.setUniform("Tex1", 0);
    aaProgram.unuse();

    return true;
}

void ControlPanel::setAALevel(int aaLevel) {
    this->aaLevel = aaLevel;

    if (currentEffect->isAntiAliasingOn()) {
        currentEffect->program->use();
        currentEffect->program->setUniform("AALevel", aaLevel);
        currentEffect->program->setUniform("InvAALevel", 1.0f / (float) aaLevel);
    }
}

void ControlPanel::setAAScale(float aaScale) {
    this->aaScale = aaScale;
    if (currentEffect->isAntiAliasingOn()) {
        currentEffect->program->use();
        currentEffect->program->setUniform("AAScale", aaScale);
    }
}

void ControlPanel::updateCallback(float msTime) {
    if (paused) {
        return;
    }
    totalTime += msTime * speed;
    currentEffect->setTime(totalTime);
    refresh();
}

void ControlPanel::mousePosCallback(int x, int y) {
    TwEventMousePosGLFW(x, y);

    if (x < widthOffset) {
        mouseOver = true;
    } else {
        mouseOver = false;
    }

    if (!mouseOver && lmbPressed && x >= widthOffset && x < windowWidth
            && y >= 0 && y < windowHeight) {

        currentEffect->mouseDragged(x - widthOffset, imageHeight - y);
    }
}

void ControlPanel::mouseButtonCallback(int id, int state) {
    TwGetParam(effectBar, NULL, "size", TW_PARAM_INT32, 2, &settingsBarBounds.width);
    TwGetParam(effectBar, NULL, "position", TW_PARAM_INT32, 2, &settingsBarBounds.x);

    if (id == GLFW_MOUSE_BUTTON_LEFT) {
        int x, y;
        glfwGetMousePos(&x, &y);

        if (state == GLFW_PRESS) {

            if (!mouseOver && x >= widthOffset && x < windowWidth
                    && y >= 0 && y < windowHeight) {
                currentEffect->mousePressed(x - widthOffset, imageHeight - y);
            }

            lmbPressed = true;
        } else {
            if (lmbPressed) {
                currentEffect->mouseReleased(x - widthOffset, imageHeight - y);
            }

            lmbPressed = false;
            mouseOver = false;
        }
    }

    TwEventMouseButtonGLFW(id, state);
}

void ControlPanel::resizeCallback(int width, int height) {
    this->windowWidth = width;
    this->windowHeight = height;
    this->imageWidth = (int) ((float) width * (X_MAX - X_MIN) / 2.0f);
    this->imageHeight = height;
    this->widthOffset = windowWidth - imageWidth;

    LOG_TRACE(logger, "resizeCallback(" << this->imageWidth << ", " << this->imageHeight << ")");

    if (imageWidth == 0 || imageHeight == 0) {
        return;
    }

    if (!texRenderTarget.create(this->imageWidth, this->imageHeight)) {
        LOG_ERROR(logger, "texRenderTarget.create() FAILED!!");
    }

    glAssert;

    if (!secondTexRenderTarget.create(this->imageWidth, this->imageHeight)) {
        LOG_ERROR(logger, "secondTexRenderTarget.create() FAILED!!");
    }
    glAssert;

    if (!aaRenderTarget.create(this->imageWidth, this->imageHeight)) {
        LOG_ERROR(logger, "aaRenderTarget.create() FAILED!!");
    }
    glAssert;

    texRenderTarget.getCurrentTexture().setSampler(
            REV_MIN_FILTER, REV_MAG_FILTER, REV_WRAP_S, REV_WRAP_T);
    secondTexRenderTarget.getCurrentTexture().setSampler(
            REV_MIN_FILTER, REV_MAG_FILTER, REV_WRAP_S, REV_WRAP_T);

    aaProgram.use();
    glAssert;

    aaProgram.setUniform("width", (float) this->imageWidth);
    glAssert;

    aaProgram.setUniform("height", (float) this->imageHeight);
    glAssert;

    aaProgram.unuse();
    glAssert;

    if (currentEffect) {
        currentEffect->resize(this->imageWidth, this->imageHeight);
    }
    glAssert;

    TwWindowSize(width, height);

}

bool ControlPanel::addEffects() {
    AbstractEffect* effect = new ReliefTunnelEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "ReliefTunnelEffect::init() FAILED!!");
        delete effect;
    } else {
        effect->use();
        effectMap["reliefTunnel"] = effect;
    }
	glAlwaysAssertNoExit;
    //----------------------------------------------------
    effect = new SquareTunnelEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "SquareTunnelEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["squareTunnel"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new MonjoriEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "MonjoriEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["monjori"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new KaleidoscopeEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "KaleidoscopeEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["kaleidoscope"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new DeformEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "DeformEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["deform"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new FlyEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "FlyEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["fly"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new PlasmaEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "PlasmaEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["plasma"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new PulseEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "PulseEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["pulse"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new WaterEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "WaterEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["water"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new ShapesEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "ShapesEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["shapes"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new MetablobEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "MetablobEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["metablob"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new SevenZeroFourEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "SevenZeroFourEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["sevenZeroFour"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new LandscapeEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "LandscapeEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["landscape"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new AppleEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "AppleEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["apple"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new TheRoadOfRibbonEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "TheRoadOfRibbonEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["theRoadOfRibbon"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new NautilusEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "NautilusEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["nautilus"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new MengerSpongeEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "MengerSpongeEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["mengerSponge"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new ClodEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "ClodEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["clod"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new SlisesixEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "SlisesixEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["slisesix"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new SultEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "SultEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["sult"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new ValleyballEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "ValleyballEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["valleyball"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new RedEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "RedEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["red"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new QuaternionEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "QuaternionEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["quaternion"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new MetaTunnelEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "MetaTunnelEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["metaTunnel"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new DroidEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "DroidEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["droid"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new LeizexEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "LeizexEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["leizex"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new KinderpainterEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "KinderpainterEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["kinderpainter"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new MandelbulbEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "MandelbulbEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["mandelbulb"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;
    effect = new DiscoEffect(this);
    if (!effect->init()) {
        LOG_ERROR(logger, "DiscoEffect::init() FAILED!!");
        delete effect;
    } else {
        effectMap["disco"] = effect;
    }
    //----------------------------------------------------
	glAlwaysAssertNoExit;

    if (effectMap.empty()) {
        return false;
    }

    return true;
}

//
//void ControlPanel::render() {
//    if (!currentEffect || texture.isInitialized() == false) {
//        TwDraw();
//        return;
//    }
//
//    glViewport(0, 0, imageWidth, imageHeight);
//    glAssert;
//
//    aaRenderTarget.use();
//    glAssert;
//
//    currentEffect->apply();
//    glAssert;
//
//    secondQuadVBO.render();
//    glAssert;
//
//    glViewport(0, 0, windowWidth, windowHeight);
//    glAssert;
//
//    aaRenderTarget.unuse();
//    glAssert;
//
//    aaProgram.use();
//    glAssert;
//
//    glActiveTexture(GL_TEXTURE0);
//    aaRenderTarget.getTexture().bind();
//    glAssert;
//
//    quadVBO.render();
//    glAssert;
//
//    aaProgram.unuse();
//    glAssert;
//
//    TwDraw();
//}

void ControlPanel::render() {
    if (!currentEffect || texture.isInitialized() == false) {
        TwDraw();
        return;
    }

    if (fxaaActive) {
        glViewport(0, 0, imageWidth, imageHeight);
        glAssert;

        aaRenderTarget.use();
        glAssert;

        currentEffect->apply();
        glAssert;

        secondQuadVBO.render();
        glAssert;

        glViewport(0, 0, windowWidth, windowHeight);
        glAssert;

        aaRenderTarget.unuse();
        glAssert;

        aaProgram.use();
        glAssert;

        glActiveTexture(GL_TEXTURE0);
        aaRenderTarget.getCurrentTexture().bind();
        glAssert;

        quadVBO.render();
        glAssert;

        aaProgram.unuse();
        glAssert;
    } else {
        currentEffect->apply();
        quadVBO.render();
    }

    TwDraw();
}

bool ControlPanel::loadBackgroundImages() {
    if (!getImagePathList("images/", backgroudImagesVector)) {
        return false;
    }

    if (backgroudImagesVector.size() == 0) {
        LOG_ERROR(logger, "There are no image files in 'images' direcotry!!");
        return false;
    }

    size_t size = backgroudImagesVector.size();

    TwEnumVal* imagesEnum = new TwEnumVal[size];
    string** filenames = new string*[size];
    int i = 0;

    for (vector<path>::iterator iter = backgroudImagesVector.begin();
            iter != backgroudImagesVector.end();) {
        path p = *(iter++);

        imagesEnum[i].Value = i;


        filenames[i] = new string(pathToString(p));
        imagesEnum[i].Label = filenames[i]->c_str();
        i++;
    }


    TwType imagesType = TwDefineEnum("BackgroundType", imagesEnum, size);
    TwAddVarCB(controlBar, "Background", imagesType, setBackgroundCallback,
            getBackgroundCallback, this, "label='Image:' ");

    for (int i = 0; i < size; i++) {
        delete filenames[i];
    }

    delete[] imagesEnum;
    delete[] filenames;

    string filename = pathToString(backgroudImagesVector[0]); //.file_string();

    if (!changeTexture(filename.c_str())) {
        return false;
    }

    return true;
}

bool ControlPanel::getImagePathList(const char* directory, vector<path>& result) {
    path p(directory);

    static string extensions[] = {".bmp", ".tga", ".png", ".jpg", ".jpeg"};
    static size_t extensionsSize = sizeof (extensions) / sizeof (string);

    try {
        if (exists(p)) {

            if (is_directory(p)) {

                for (directory_iterator iter = directory_iterator(p);
                        iter != directory_iterator();) {
                    path p1 = *(iter++);

                    if (!is_regular_file(p1)) {
                        continue;
                    }

                    for (int i = 0; i < extensionsSize; i++) {
                        path extPath = p1.extension();
                        const wchar_t* wExt = extPath.c_str();
                        string ext = StringUtil::fromWideString(wExt);
                        const char* ext1 = extensions[i].c_str();
                        const char* ext2 = ext.c_str();
                        if (strcmp(ext1, ext2) == 0) {
                            result.push_back(p1);
                            break;
                        }
                    }
                }
            } else {
                LOG_ERROR(logger, "'" << p << "' is not a directory!!");
                return false;
            }

        } else {
            LOG_ERROR(logger, "'" << p << "' does not exist!!");
            return false;
        }
    } catch (const filesystem_error& ex) {
        LOG_ERROR(logger, ex.what());
        return false;
    }

    return true;
}

void TW_CALL ControlPanel::setBackgroundCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->selectedBackgroundIndex = *(const int*) value;
    string filename = pathToString(controlPanel->backgroudImagesVector[controlPanel->selectedBackgroundIndex]);

    controlPanel->changeTexture(filename.c_str());
}

void TW_CALL ControlPanel::getBackgroundCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(int*) value = controlPanel->selectedBackgroundIndex;
}

void TW_CALL ControlPanel::resetButtonCallback(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    LOG_TRACE(logger, "Reset current filter ");

    if (controlPanel->currentEffect != NULL) {
        controlPanel->currentEffect->reset();
    }
}

bool ControlPanel::changeTexture(const char* textureFile) {

    texture.load(textureFile);

    if (texture.isInitialized() == false) {
        LOG_ERROR(logger, "Loading texture '" << textureFile << "' FAILED!");
        return false;
    }

    LOG_INFO(logger, "Texture '" << textureFile << "' loaded successfully.");

    return true;
}

// ---------------------------------------------------------------------------
// 2) Callback functions for C-Dynamic string variables
// ---------------------------------------------------------------------------

// Function called to copy the content of a C-Dynamic String (src) handled by
// the AntTweakBar library to a C-Dynamic string (*destPtr) handled by our application

void TW_CALL ControlPanel::copyCDStringToClient(char** destPtr, const char* src) {
    size_t srcLen = (src != NULL) ? strlen(src) : 0;
    size_t destLen = (*destPtr != NULL) ? strlen(*destPtr) : 0;

    // Alloc or realloc dest memory block if needed
    if (*destPtr == NULL)
        *destPtr = (char *) malloc(srcLen + 1);
    else if (srcLen > destLen)
        *destPtr = (char *) realloc(*destPtr, srcLen + 1);

    // Copy src
    if (srcLen > 0)
        strncpy(*destPtr, src, srcLen);
    (*destPtr)[srcLen] = '\0'; // null-terminated string
}

void ControlPanel::setEffect(AbstractEffect* effect) {
    this->currentEffect = effect;
    copyCDStringToClient(&currentEffectStr, effect->getName());

    LOG_INFO(logger, "Using effect: " << currentEffect->getName());

    setAAScale(aaScale);
    setAALevel(aaLevel);

    aaOn = this->currentEffect->isAntiAliasingOn();
    if (!aaOn) {
        TwDefine(" Control/__AALevel readonly=true ");
        TwDefine(" Control/__AAScale  readonly=true ");
    } else {
        TwDefine(" Control/__AALevel readonly=false ");
        TwDefine(" Control/__AAScale readonly=false ");
    }

    TwRefreshBar(controlBar);
}

void TW_CALL ControlPanel::setAALevelCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->setAALevel(*(const int*) value);
}

void TW_CALL ControlPanel::getAALevelCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(int*) value = controlPanel->getAALevel();
}

void TW_CALL ControlPanel::setAAScaleCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->setAAScale(*(const float*) value);
}

void TW_CALL ControlPanel::getAAScaleCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->getAAScale();
}

void TW_CALL ControlPanel::resetTimeCallback(void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
    controlPanel->totalTime = 0.0f;

    controlPanel->currentEffect->setTime(controlPanel->totalTime);
    controlPanel->refresh();
}

void TW_CALL ControlPanel::setFxaaSpanMaxCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->fxaaSpanMax = *(const float*) value;
    controlPanel->aaProgram.use();
    controlPanel->aaProgram.setUniform("FXAA_SPAN_MAX", controlPanel->fxaaSpanMax);
    controlPanel->aaProgram.unuse();
}

void TW_CALL ControlPanel::getFxaaSpanMaxCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->fxaaSpanMax;
}

void TW_CALL ControlPanel::setFxaaReduceMulCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->fxaaReduceMul = *(const float*) value;
    controlPanel->aaProgram.use();
    controlPanel->aaProgram.setUniform("FXAA_REDUCE_MUL", controlPanel->fxaaReduceMul);
    controlPanel->aaProgram.unuse();
}

void TW_CALL ControlPanel::getFxaaReduceMulCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->fxaaReduceMul;
}
//
//void TW_CALL ControlPanel::setFxaaVxOffsetCallback(const void* value, void* clientData) {
//    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
//
//    controlPanel->fxaaVxOffset = *(const float*) value;
//    controlPanel->aaProgram.use();
//    controlPanel->aaProgram.setUniform("vx_offset", controlPanel->fxaaVxOffset);
//    controlPanel->aaProgram.unuse();
//}
//
//void TW_CALL ControlPanel::getFxaaVxOffsetCallback(void* value, void* clientData) {
//    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);
//
//    *(float*) value = controlPanel->fxaaVxOffset;
//}

void TW_CALL ControlPanel::setFxaaSubpixShiftCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->fxaaSubpixShift = *(const float*) value;
    controlPanel->aaProgram.use();
    controlPanel->aaProgram.setUniform("FXAA_SUBPIX_SHIFT", controlPanel->fxaaSubpixShift);
    controlPanel->aaProgram.unuse();
}

void TW_CALL ControlPanel::getFxaaSubpixShiftCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->fxaaSubpixShift;
}