#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$
/**
 * 
 * Fragment shader from "Shader Toy"  by Inigo Quilez and others 
 * (http://www.iquilezles.org/apps/shadertoy/).
 * 
 */
#version 150

in vec2 TexCoord;

uniform sampler2D Tex1;
uniform float Time = 0.0f;

uniform float AAScale;
uniform int AALevel;
uniform float InvAALevel;

out vec4 FragColor;

vec3 getPixelColor(vec2 p) {
    vec2 uv;

    float r = pow(pow(p.x * p.x, 16.0) + pow(p.y * p.y, 16.0), 1.0 / 32.0);
    uv.x = .5 * Time + 0.5 / r;
    uv.y = 1.0 * atan(p.y, p.x) / 3.1416;

    vec3 col = texture(Tex1, uv).xyz;

    return col * r * r * r;
}

void main() {
    vec2 p = -1.0 + 2.0 * TexCoord;

    if (AALevel == 1) {
        FragColor = vec4(getPixelColor(p), 1.0f);
    } else {
        vec3 color = vec3(0.0, 0.0, 0.0);

        for (int x = 0; x < AALevel; x++) {
            for (int y = 0; y < AALevel; y++) {
                color += getPixelColor(p + vec2(x, y) * InvAALevel * AAScale);
            }
        }

        FragColor = vec4(color * InvAALevel * InvAALevel, 1.0f);
    }
}

