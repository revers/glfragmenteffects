#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$
/**
 * 
 * Fragment shader from "Shader Toy"  by Inigo Quilez and others 
 * (http://www.iquilezles.org/apps/shadertoy/).
 * 
 */
#version 150

in vec2 TexCoord;

uniform sampler2D Tex1;
uniform float Time = 0.0f;
uniform vec2 Resolution = vec2(800, 600);

out vec4 FragColor;

const float PI = 3.1415926535897932;

//speed
const float speed = 0.2;
const float speed_x = 0.3;
const float speed_y = 0.3;

// geometry
uniform float intensity = 3.0;
uniform int steps = 8;
uniform float frequency = 4.0;
uniform int angle = 7; // better when a prime

// reflection and emboss
uniform float delta = 20.0;
uniform float intence = 400.0;
uniform float emboss = 0.3;

//---------- crystals effect

float col(vec2 coord) {
    float delta_theta = 2.0 * PI / float(angle);
    float col = 0.0;
    float theta = 0.0;
    for (int i = 0; i < steps; i++) {
        vec2 adjc = coord;
        theta = delta_theta * float(i);
        adjc.x += cos(theta) * Time * speed + Time * speed_x;
        adjc.y -= sin(theta) * Time * speed - Time * speed_y;
        col = col + cos((adjc.x * cos(theta) - adjc.y * sin(theta)) * frequency) * intensity;
    }

    return cos(col);
}

void main() {
    vec2 p = (gl_FragCoord.xy) / Resolution;
    vec2 c1 = p;
    vec2 c2 = p;
    float cc1 = col(c1);

    c2.x += Resolution.x / delta;
    float dx = emboss * (cc1 - col(c2)) / delta;

    c2.x = p.x;
    c2.y += Resolution.y / delta;
    float dy = emboss * (cc1 - col(c2)) / delta;

    c1.x += dx;
    c1.y += dy;// -(c1.y + dy);

    float alpha = 1. + dot(dx, dy) * intence;
    FragColor = texture(Tex1, c1)*(alpha);
}
