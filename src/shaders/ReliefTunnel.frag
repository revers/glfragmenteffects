#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$
/**
 * 
 * Fragment shader from "Shader Toy"  by Inigo Quilez and others 
 * (http://www.iquilezles.org/apps/shadertoy/).
 * 
 */
#version 150

in vec2 TexCoord;

uniform sampler2D Tex1;
uniform float Time = 0.0f;

uniform float AAScale;
uniform int AALevel;
uniform float InvAALevel;

out vec4 FragColor;

vec3 getPixelColor(vec2 p) {
    vec2 uv;

    float r = sqrt(dot(p, p));
    float a = atan(p.x, p.y) + 0.5 * sin(0.5 * r - 0.5 * Time);
    
    float s = 0.5 + 0.5 * cos(7.0 * a);
    s = smoothstep(0.0, 1.0, s);
    s = smoothstep(0.0, 1.0, s);
    s = smoothstep(0.0, 1.0, s);
    s = smoothstep(0.0, 1.0, s);

    uv.x = Time + 1.0 / (r + 0.2 * s);
    uv.y = 3.0 * a / 3.1416;

    float w = (0.5 + 0.5 * s) * r * r;

    vec3 col = texture(Tex1, uv).xyz;

    float ao = 0.5 + 0.5 * cos(7.0 * a);
    ao = smoothstep(0.0, 0.4, ao) - smoothstep(0.4, 0.7, ao);
    ao = 1.0 - 0.5 * ao * r;

    return col * w * ao;
}

void main() {
    vec2 p = -1.0 + 2.0 * TexCoord;

    if (AALevel == 1) {
        FragColor = vec4(getPixelColor(p), 1.0f);
    } else {
        vec3 color = vec3(0.0, 0.0, 0.0);
        
        for (int x = 0; x < AALevel; x++) {
            for (int y = 0; y < AALevel; y++) {
                color += getPixelColor(p + vec2(x, y) * InvAALevel * AAScale);
            }
        }

        FragColor = vec4(color * InvAALevel * InvAALevel, 1.0f);
    }
}
