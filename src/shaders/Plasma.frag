#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$
/**
 * 
 * Fragment shader from "Shader Toy"  by Inigo Quilez and others 
 * (http://www.iquilezles.org/apps/shadertoy/).
 * 
 */
#version 150

in vec2 TexCoord;

uniform sampler2D Tex1;
uniform float Time = 0.0f;
uniform float Factor = 500.0f;

uniform float AAScale;
uniform int AALevel;
uniform float InvAALevel;

uniform vec2 Resolution = vec2(800, 600);

out vec4 FragColor;

vec3 getPixelColor(vec2 p) {
    //    float x = gl_FragCoord.x;
    //    float y = gl_FragCoord.y;
    float mov0 = p.x * Factor + p.y * Factor + cos(sin(Time)*2.)*100.
            + sin(p.x * Factor / 100.)*1000.;
    float mov1 = p.y / 0.2 + Time;
    float mov2 = p.x / 0.2;
    float c1 = abs(sin(mov1 + Time) / 2. + mov2 / 2. - mov1 - mov2 + Time);
    float c2 = abs(sin(c1 + sin(mov0 / 1000. + Time) + sin(p.y * Factor / 40. + Time)
            + sin((p.x * Factor + p.y * Factor) / 100.)*3.));
    float c3 = abs(sin(c2 + cos(mov1 + mov2 + c2) + cos(mov2)
            + sin(p.x * Factor / 1000.)));

    return vec3(c1, c2, c3);
}

void main() {
    vec2 p = -1.0 + 2.0 * TexCoord;

    if (AALevel == 1) {
        FragColor = vec4(getPixelColor(p), 1.0f);
    } else {
        vec3 color = vec3(0.0, 0.0, 0.0);

        for (int x = 0; x < AALevel; x++) {
            for (int y = 0; y < AALevel; y++) {
                color += getPixelColor(p + vec2(x, y) * InvAALevel * AAScale);
            }
        }

        FragColor = vec4(color * InvAALevel * InvAALevel, 1.0f);
    }
}
