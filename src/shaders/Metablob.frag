#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$
/**
 * 
 * Fragment shader from "Shader Toy"  by Inigo Quilez and others 
 * (http://www.iquilezles.org/apps/shadertoy/).
 * 
 */
#version 150

in vec2 TexCoord;

uniform float Time = 0.0f;

uniform float AAScale;
uniform int AALevel;
uniform float InvAALevel;

out vec4 FragColor;

vec3 getPixelColor(vec2 p) {

    //the centre point for each blob
    vec2 move1;
    move1.x = cos(Time)*0.4;
    move1.y = sin(Time * 1.5) * 0.4;
    vec2 move2;
    move2.x = cos(Time * 2.0) * 0.4;
    move2.y = sin(Time * 3.0) * 0.4;

    //radius for each blob
    float r1 = (dot(p - move1, p - move1)) * 8.0;
    float r2 = (dot(p + move2, p + move2)) * 16.0;

    //sum the meatballs
    float metaball = (1.0 / r1 + 1.0 / r2);
    //alter the cut-off power
    float col = pow(metaball, 8.0);

    //set the output color
    return vec3(col, col, col);
}

void main() {
    vec2 p = -1.0 + 2.0 * TexCoord;

    if (AALevel == 1) {
        FragColor = vec4(getPixelColor(p), 1.0f);
    } else {
        vec3 color = vec3(0.0, 0.0, 0.0);

        for (int x = 0; x < AALevel; x++) {
            for (int y = 0; y < AALevel; y++) {
                color += getPixelColor(p + vec2(x, y) * InvAALevel * AAScale);
            }
        }

        FragColor = vec4(color * InvAALevel * InvAALevel, 1.0f);
    }
}


