#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$
/**
 * 
 * Fragment shader from "Shader Toy"  by Inigo Quilez and others 
 * (http://www.iquilezles.org/apps/shadertoy/).
 * 
 */
#version 150

in vec2 TexCoord;
uniform float Time = 0.0f;

//uniform float AspectRatio = 1.0f;

uniform float AAScale;
uniform int AALevel = 1;
uniform float InvAALevel;

out vec4 FragColor;

float h(vec3 q) {
    float f = 1. * distance(q, vec3(cos(Time) + sin(Time * .2), .3, 2. + cos(Time * .5)*.5));
    f *= distance(q, vec3(-cos(Time * .7), .3, 2. + sin(Time * .5)));
    f *= distance(q, vec3(-sin(Time * .2)*.5, sin(Time), 2.));
    f *= cos(q.y) * cos(q.x) - .1 - cos(q.z * 7. + Time * 7.) * cos(q.x * 3.) * cos(q.y * 4.)*.1;
    return f;
}

vec4 getPixelColor(vec2 p) {
    //vec2 p = -1.0 + 2.0 * gl_FragCoord.xy / resolution.xy;
    vec3 o = vec3(p.x, p.y * 1.25 - 0.3, 0.);
    vec3 d = vec3(p.x + cos(Time)*0.3, p.y, 1.) / 64.;
    vec4 c = vec4(0.);
    float t = 0.;
    for (int i = 0; i < 75; i++) {
        if (h(o + d * t) < .4) {
            t -= 5.;
            for (int j = 0; j < 5; j++) {
                if (h(o + d * t) < .4)
                    break;
                t += 1.;
            }
            vec3 e = vec3(.01, .0, .0);
            vec3 n = vec3(.0);
            n.x = h(o + d * t) - h(vec3(o + d * t + e.xyy));
            n.y = h(o + d * t) - h(vec3(o + d * t + e.yxy));
            n.z = h(o + d * t) - h(vec3(o + d * t + e.yyx));
            n = normalize(n);
            c += max(dot(vec3(.0, .0, -.5), n), .0) + max(dot(vec3(.0, -.5, .5), n), .0)*.5;
            break;
        }
        t += 5.;
    }
    return c + vec4(.1, .2, .5, 1.)*(t * .025);
}

void main() {
    vec2 p = -1.0 + 2.0 * TexCoord;

    if (AALevel == 1) {
        FragColor = getPixelColor(p);
    } else {
        vec4 color = vec4(0.0, 0.0, 0.0, 0.0);

        for (int x = 0; x < AALevel; x++) {
            for (int y = 0; y < AALevel; y++) {
                color += getPixelColor(p + vec2(x, y) * InvAALevel * AAScale);
            }
        }

        FragColor = color * InvAALevel * InvAALevel;
        FragColor.w = 1.0f;
    }
}

