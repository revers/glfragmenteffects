#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$
/**
 * 
 * Fragment shader from "Shader Toy"  by Inigo Quilez and others 
 * (http://www.iquilezles.org/apps/shadertoy/).
 * 
 */
#version 150

in vec2 TexCoord;

uniform sampler2D Tex1;
uniform float Time = 0.0;
uniform vec2 Mouse = vec2(0.0, 0.0);

uniform float AAScale;
uniform int AALevel;
uniform float InvAALevel;

out vec4 FragColor;

vec3 getPixelColor(vec2 p) {
    float a1 = atan(p.y - Mouse.y, p.x - Mouse.x);
    float r1 = sqrt(dot(p - Mouse, p - Mouse));
    float a2 = atan(p.y + Mouse.y, p.x + Mouse.x);
    float r2 = sqrt(dot(p + Mouse, p + Mouse));

    vec2 uv;
    uv.x = 0.2 * Time + (r1 - r2) * 0.25;
    uv.y = sin(2.0 * (a1 - a2));

    float w = r1 * r2 * 0.8;
    vec3 col = texture(Tex1, uv).xyz;

    return col / (0.1 + w);

}

void main() {
    vec2 p = -1.0 + 2.0 * TexCoord;

    if (AALevel == 1) {
        FragColor = vec4(getPixelColor(p), 1.0f);
    } else {
        vec3 color = vec3(0.0, 0.0, 0.0);

        for (int x = 0; x < AALevel; x++) {
            for (int y = 0; y < AALevel; y++) {
                color += getPixelColor(p + vec2(x, y) * InvAALevel * AAScale);
            }
        }

        FragColor = vec4(color * InvAALevel * InvAALevel, 1.0f);
    }
}
