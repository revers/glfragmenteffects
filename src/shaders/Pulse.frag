#include <FAKE_INCLUDE/glsl_fake.hxx>
//$END_FAKE$
/**
 * 
 * Fragment shader from "Shader Toy"  by Inigo Quilez and others 
 * (http://www.iquilezles.org/apps/shadertoy/).
 * 
 */
#version 150

in vec2 TexCoord;

uniform sampler2D Tex1;
uniform float Time = 0.0f;
uniform vec2 Resolution = vec2(800, 600);
uniform vec2 Mouse = vec2(0, 0);

out vec4 FragColor;

void main() {
    vec2 halfres = Resolution * 0.5;
    vec2 cPos = gl_FragCoord.xy;
    
    cPos.x -= 0.5 * halfres.x * sin(Time / 2.0) + 0.3 * halfres.x * cos(Time) + halfres.x + Mouse.x;
    cPos.y -= 0.4 * halfres.y * sin(Time / 5.0) + 0.3 * halfres.y * cos(Time) + Mouse.y;
   
    float cLength = length(cPos);

    vec2 uv = vec2(0.5) + gl_FragCoord.xy / Resolution + (cPos / cLength) * sin(cLength / 30.0 - Time * 10.0) / 25.0;

    FragColor = vec4(texture(Tex1, uv).xyz * 50.0 / cLength, 1.0f);
}

