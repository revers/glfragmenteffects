/* 
 * File:   MyCLFractals.cpp
 * Author: Revers
 *
 * Created on 17 marzec 2012, 15:54
 * 
 * check: http://www.iquilezles.org/apps/shadertoy/
 */

#include "LoggingDefines.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <iostream>

#include <GL/glew.h>
#include <GL/glfw.h>
#include <GL/gl.h>

#include "ControlPanel.h"

#include <log4cplus/configurator.h>
#include <log4cplus/helpers/loglog.h>
#include <log4cplus/loggingmacros.h>
#include <log4cplus/consoleappender.h>

#include <rev/common/RevFPSCounter.hpp>

#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevIErrorStreamListener.h>

using namespace rev;
using namespace std;
using namespace log4cplus;

int running = 1;

//------------------------------------------------
// Defines:
//------------------------------------------------
#ifdef NDEBUG
#define APP_TITLE "GLFragmentEffects"
#else
#define APP_TITLE "GLFragmentEffects (DEBUG)"
#endif

#define FAILURE 1
#define SUCCESS 0

#define WINDOW_WIDTH 1200
#define WINDOW_HEIGHT 600

//------------------------------------------------
// Globals:
//------------------------------------------------
bool lmbPressed = false;
int lastMouseWheelPos = 0;
int lastX = 0;
int lastY = 0;

bool wireFrameMode = false;

FPSCounter fpsCounter(1.0f / 60.0f);
ControlPanel controlPanel;

Logger mainLogger = Logger::getInstance("mainLogger");

void renderScene() {
    glClear(GL_COLOR_BUFFER_BIT);

    if (wireFrameMode) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    controlPanel.render();

    glfwSwapBuffers();
}

void GLFWCALL keyDownCallback(int key, int action) {
    if (action != GLFW_PRESS) {
        return;
    }

    switch (key) {
        case GLFW_KEY_ESC:
            running = 0;
            break;
        case GLFW_KEY_SPACE:
            controlPanel.paused = !controlPanel.paused;
            //wireFrameMode = !wireFrameMode;
            break;

        case 'a':
        case 'A':
        {

        }
            break;

        default:
            break;
    }
}

void GLFWCALL resizeCallback(int width, int height) {
    controlPanel.resizeCallback(width, height);
    float ratio = 1.0f;

    if (height > 0) {
        ratio = (float) width / (float) height;
    }

    glViewport(0, 0, width, height);
}

void GLFWCALL mousePosCallback(int x, int y) {

    controlPanel.mousePosCallback(x, y);

    if (lmbPressed) {

        int xDiff = x - lastX;
        int yDiff = y - lastY;

        controlPanel.refresh();

        lastX = x;
        lastY = y;
    }
}

void GLFWCALL mouseButtonCallback(int id, int state) {

    controlPanel.mouseButtonCallback(id, state);

    if (id == GLFW_MOUSE_BUTTON_LEFT) {
        if (state == GLFW_PRESS) {
            int x, y;
            glfwGetMousePos(&x, &y);

            lastX = x;
            lastY = y;
            if (!controlPanel.mouseOver) {
                lmbPressed = true;
            }
        } else {
            lmbPressed = false;
        }
    }
}

void GLFWCALL mouseWheelCallback(int pos) {
    controlPanel.mouseWheelCallback(pos);
}

int initializeGL() {

    glDisable(GL_DEPTH_TEST);

    glClearColor(0, 0, 0, 1);

    return SUCCESS;
}

#define LOG_PROPERTIES_FILE "log4cplus.properties"

bool initializeLogger() {

    std::fstream file(LOG_PROPERTIES_FILE);
    if (!file) {
        Logger root = Logger::getRoot();
        SharedAppenderPtr consoleAppender(new ConsoleAppender());
        consoleAppender->setName("mainConsoleAppender");

        std::auto_ptr<Layout> layout = std::auto_ptr<Layout > (new log4cplus::TTCCLayout());
        consoleAppender->setLayout(layout);
        root.addAppender(consoleAppender);

        LOG_WARN(mainLogger, "File '" << LOG_PROPERTIES_FILE
                << "' does not exist!!");

        return true;
    }

    try {
        PropertyConfigurator::doConfigure(LOG_PROPERTIES_FILE);
    } catch (...) {
        cout << "ERROR: Properties configuration FAILED!!" << endl;
        return false;
    }

    LOG_INFO(mainLogger, "'" << LOG_PROPERTIES_FILE
            << "' file loadded successfully.");

    return true;
}

class LogErrorStreamListener : public rev::IErrorStreamListener {
public:

    /**
     * @Override
     */
    virtual void print(const char* line) {
        LOG_ERROR(mainLogger, line);
    }
};

int main(int argc, char* argv[]) {
    srand(time(0));

    if (!initializeLogger()) {
        cout << "ERROR: initializeLogger() FAILED!" << endl;
        return -1;
    }

    LogErrorStreamListener logErrStreamListener;
    rev::errout.addErrorStreamListener(&logErrStreamListener);
    rev::errout.setUseStdout(false); // only log stream

    if (glfwInit() == GL_FALSE) {
        fprintf(stderr, "GLFW initialization failed\n");
        exit(-1);
    }

    int width = WINDOW_WIDTH;
    int height = WINDOW_HEIGHT;

    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 2);
    glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwOpenWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    GLFWvidmode mode;
    glfwGetDesktopMode(&mode);

    if (glfwOpenWindow(width, height, mode.RedBits, mode.GreenBits, mode.BlueBits, 0, 16, 0, GLFW_WINDOW /* or GLFW_FULLSCREEN */) == GL_FALSE) {
        fprintf(stderr, "Could not open window\n");
        glfwTerminate();
        exit(-1);
    }

    glewInit();

    glAlwaysAssertNoExit;

    glfwSetWindowTitle(APP_TITLE);
    glfwSwapInterval(1);
    glAlwaysAssert;

    if (controlPanel.init() == false) {
        LOG_ERROR(mainLogger, "ControlPanel::init() FAILED!");
        return -1;
    }

    glAlwaysAssert;

    glfwSetKeyCallback(keyDownCallback);
    glfwSetMousePosCallback(mousePosCallback);
    glfwSetMouseButtonCallback(mouseButtonCallback);
    glfwSetMouseWheelCallback(mouseWheelCallback);
    glfwSetWindowSizeCallback(resizeCallback);
    glfwSetCharCallback((GLFWcharfun) TwEventCharGLFW);

    glfwEnable(GLFW_KEY_REPEAT);

    if (initializeGL() == FAILURE) {
        cout << "ERROR: initializeGL FAILED!" << endl;
        return -1;
    }

    glAlwaysAssert;

    glfwSwapInterval(0);
    LOG_INFO(mainLogger, "\n" << rev::GLUtil::getGLInfo(false));

    while (running) {

        fpsCounter.frameBegin();

        renderScene();

        if (fpsCounter.frameEnd()) {
            controlPanel.updateCallback(fpsCounter.getFrameTime());

            char title[256];
            sprintf(title, APP_TITLE " | Filter: %s | %d fps ",
                    controlPanel.getCurrentEffectName(),
                    fpsCounter.getFramesPerSec());

            glfwSetWindowTitle(title);
        }

        running = running && glfwGetWindowParam(GLFW_OPENED);
    }

    glfwTerminate();

    return 0;
}


