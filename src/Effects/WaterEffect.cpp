/* 
 * File:   WaterEffect.cpp
 * Author: Revers
 * 
 * Created on 18 czerwiec 2012, 20:31
 */

#include <GL/glew.h>
#include <GL/gl.h>

#include "WaterEffect.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define EFFECT_GROUP "Water"
#define EFFECT_PREFIX "water_"
#define VERTEX_SHADER_FILE "shaders/Default.vert"
#define FRAGMENT_SHADER_FILE "shaders/Water.frag"

#define UNIFORM_INTENSITY "intensity"
#define UNIFORM_STEPS "steps"
#define UNIFORM_FREQUENCY "frequency"
#define UNIFORM_ANGLE "angle"
#define UNIFORM_DELTA "delta"
#define UNIFORM_INTENCE "intence"
#define UNIFORM_EMBOSS "emboss"

#define DEFAULT_INTENSITY 3.0f
#define DEFAULT_STEPS 8
#define DEFAULT_FREQUENCY 4.0f
#define DEFAULT_ANGLE 7
#define DEFAULT_DELTA 20.0f
#define DEFAULT_INTENCE 400.0f
#define DEFAULT_EMBOSS 0.3f

Logger WaterEffect::logger = Logger::getInstance("effects.Water");

WaterEffect::WaterEffect(ControlPanel* controlPanel)
: AbstractEffect(controlPanel) {
    antiAliasingOn = false;

    intensity = DEFAULT_INTENSITY;
    steps = DEFAULT_STEPS;
    frequency = DEFAULT_FREQUENCY;
    angle = DEFAULT_ANGLE;
    delta = DEFAULT_DELTA;
    intence = DEFAULT_INTENCE;
    emboss = DEFAULT_EMBOSS;
}

bool WaterEffect::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderFromFile(VERTEX_SHADER_FILE,
            GLSLShader::VERTEX)) {
        LOG_ERROR(logger, "Compilation of file " VERTEX_SHADER_FILE " FAILED!!");
        return false;
    }

    if (!program->compileShaderFromFile(FRAGMENT_SHADER_FILE,
            GLSLShader::FRAGMENT)) {
        LOG_ERROR(logger, "Compilation of file " FRAGMENT_SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking program FAILED!!");
        return false;
    }

    TwAddVarCB(effectBar, EFFECT_PREFIX UNIFORM_ANGLE, TW_TYPE_INT32,
            setAngleCallback, getAngleCallback, this,
            "label='" UNIFORM_ANGLE ":' min=-10 max=20 step=1 group='"
            EFFECT_GROUP "' ");
    
    TwAddVarCB(effectBar, EFFECT_PREFIX UNIFORM_STEPS, TW_TYPE_INT32,
            setStepsCallback, getStepsCallback, this,
            "label='" UNIFORM_STEPS ":' min=0 max=100 step=1 group='"
            EFFECT_GROUP "' ");
    
    TwAddVarCB(effectBar, EFFECT_PREFIX UNIFORM_INTENSITY, TW_TYPE_FLOAT,
            setIntensityCallback, getIntensityCallback, this,
            "label='" UNIFORM_INTENSITY ":' min=0.0 max=30.0 step=0.1 group='"
            EFFECT_GROUP "' ");
    
    TwAddVarCB(effectBar, EFFECT_PREFIX UNIFORM_FREQUENCY, TW_TYPE_FLOAT,
            setFrequencyCallback, getFrequencyCallback, this,
            "label='" UNIFORM_FREQUENCY ":' min=0.0 max=30.0 step=0.1 group='"
            EFFECT_GROUP "' ");
    
    TwAddVarCB(effectBar, EFFECT_PREFIX UNIFORM_DELTA, TW_TYPE_FLOAT,
            setDeltaCallback, getDeltaCallback, this,
            "label='" UNIFORM_DELTA ":' min=0.0 max=100.0 step=1.0 group='"
            EFFECT_GROUP "' ");
    
    TwAddVarCB(effectBar, EFFECT_PREFIX UNIFORM_INTENCE, TW_TYPE_FLOAT,
            setIntenceCallback, getIntenceCallback, this,
            "label='" UNIFORM_INTENCE ":' min=0.0 max=1000.0 step=10.0 group='"
            EFFECT_GROUP "' ");
    
    TwAddVarCB(effectBar, EFFECT_PREFIX UNIFORM_EMBOSS, TW_TYPE_FLOAT,
            setEmbossCallback, getEmbossCallback, this,
            "label='" UNIFORM_EMBOSS ":' min=0.0 max=10.0 step=0.01 group='"
            EFFECT_GROUP "' ");
    

    addLoadButton(EFFECT_PREFIX "effect", EFFECT_GROUP);

    int opened = 0;
    TwSetParam(effectBar, EFFECT_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    program->use();
    program->setUniform(UNIFORM_INTENSITY, intensity);
    program->setUniform(UNIFORM_STEPS, steps);
    program->setUniform(UNIFORM_FREQUENCY, frequency);
    program->setUniform(UNIFORM_ANGLE, angle);
    program->setUniform(UNIFORM_DELTA, delta);
    program->setUniform(UNIFORM_INTENCE, intence);
    program->setUniform(UNIFORM_EMBOSS, emboss);

    return true;
}

void WaterEffect::reset() {
    if (!program) {
        return;
    }

    program->use();
    intensity = DEFAULT_INTENSITY;
    steps = DEFAULT_STEPS;
    frequency = DEFAULT_FREQUENCY;
    angle = DEFAULT_ANGLE;
    delta = DEFAULT_DELTA;
    intence = DEFAULT_INTENCE;
    emboss = DEFAULT_EMBOSS;

    program->setUniform(UNIFORM_INTENSITY, intensity);
    program->setUniform(UNIFORM_STEPS, steps);
    program->setUniform(UNIFORM_FREQUENCY, frequency);
    program->setUniform(UNIFORM_ANGLE, angle);
    program->setUniform(UNIFORM_DELTA, delta);
    program->setUniform(UNIFORM_INTENCE, intence);
    program->setUniform(UNIFORM_EMBOSS, emboss);

    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* WaterEffect::getName() {
    return EFFECT_GROUP;
}

void WaterEffect::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setEffect(this);
}

void WaterEffect::resize(int x, int y) {
    program->use();
    program->setUniform("Resolution", glm::vec2(x, y));
}

void TW_CALL
WaterEffect::setFrequencyCallback(const void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);
    effect->frequency = *(const float*) value;

    if (!effect->program || !effect->amILoaded()) {
        return;
    }

    effect->program->use();
    effect->program->setUniform(UNIFORM_FREQUENCY, effect->frequency);
}

void TW_CALL
WaterEffect::getFrequencyCallback(void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);

    *(float*) value = effect->frequency;
}

void TW_CALL
WaterEffect::setEmbossCallback(const void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);
    effect->emboss = *(const float*) value;

    if (!effect->program || !effect->amILoaded()) {
        return;
    }

    effect->program->use();
    effect->program->setUniform(UNIFORM_EMBOSS, effect->emboss);
}

void TW_CALL
WaterEffect::getEmbossCallback(void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);

    *(float*) value = effect->emboss;
}

void TW_CALL
WaterEffect::setDeltaCallback(const void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);
    effect->delta = *(const float*) value;

    if (!effect->program || !effect->amILoaded()) {
        return;
    }

    effect->program->use();
    effect->program->setUniform(UNIFORM_DELTA, effect->delta);
}

void TW_CALL
WaterEffect::getDeltaCallback(void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);

    *(float*) value = effect->delta;
}

void TW_CALL
WaterEffect::setIntensityCallback(const void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);
    effect->intensity = *(const float*) value;

    if (!effect->program || !effect->amILoaded()) {
        return;
    }

    effect->program->use();
    effect->program->setUniform(UNIFORM_INTENSITY, effect->intensity);
}

void TW_CALL
WaterEffect::getIntensityCallback(void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);

    *(float*) value = effect->intensity;
}

void TW_CALL
WaterEffect::setIntenceCallback(const void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);
    effect->intence = *(const float*) value;

    if (!effect->program || !effect->amILoaded()) {
        return;
    }

    effect->program->use();
    effect->program->setUniform(UNIFORM_INTENCE, effect->intence);
}

void TW_CALL
WaterEffect::getIntenceCallback(void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);

    *(float*) value = effect->intence;
}

void TW_CALL
WaterEffect::setAngleCallback(const void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);
    effect->angle = *(const int*) value;

    if (!effect->program || !effect->amILoaded()) {
        return;
    }

    effect->program->use();
    effect->program->setUniform(UNIFORM_ANGLE, effect->angle);
}

void TW_CALL
WaterEffect::getAngleCallback(void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);

    *(int*) value = effect->angle;
}

void TW_CALL
WaterEffect::setStepsCallback(const void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);
    effect->steps = *(const int*) value;

    if (!effect->program || !effect->amILoaded()) {
        return;
    }

    effect->program->use();
    effect->program->setUniform(UNIFORM_STEPS, effect->steps);
}

void TW_CALL
WaterEffect::getStepsCallback(void* value, void* clientData) {
    WaterEffect* effect = static_cast<WaterEffect*> (clientData);

    *(int*) value = effect->steps;
}