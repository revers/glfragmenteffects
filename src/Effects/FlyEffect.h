/* 
 * File:   FlyEffect.h
 * Author: Revers
 *
 * Created on 18 czerwiec 2012, 19:18
 */

#ifndef FLYEFFECT_H
#define	FLYEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class FlyEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    FlyEffect(ControlPanel* controlPanel);

    virtual ~FlyEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
};

#endif	/* FLYEFFECT_H */

