/* 
 * File:   KaleidoscopeEffect.cpp
 * Author: Revers
 * 
 * Created on 18 czerwiec 2012, 07:55
 */

#include <GL/glew.h>
#include <GL/gl.h>

#include "KaleidoscopeEffect.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define EFFECT_GROUP "Kaleidoscope"
#define EFFECT_PREFIX "kalei_"
#define VERTEX_SHADER_FILE "shaders/Default.vert"
#define FRAGMENT_SHADER_FILE "shaders/Kaleidoscope.frag"

Logger KaleidoscopeEffect::logger = Logger::getInstance("effects.Kaleidoscope");

KaleidoscopeEffect::KaleidoscopeEffect(ControlPanel* controlPanel)
: AbstractEffect(controlPanel) {
}

bool KaleidoscopeEffect::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderFromFile(VERTEX_SHADER_FILE,
            GLSLShader::VERTEX)) {
        LOG_ERROR(logger, "Compilation of file " VERTEX_SHADER_FILE " FAILED!!");
        return false;
    }

    if (!program->compileShaderFromFile(FRAGMENT_SHADER_FILE,
            GLSLShader::FRAGMENT)) {
        LOG_ERROR(logger, "Compilation of file " FRAGMENT_SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking program FAILED!!");
        return false;
    }

    addLoadButton(EFFECT_PREFIX "effect", EFFECT_GROUP);

    int opened = 0;
    TwSetParam(effectBar, EFFECT_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void KaleidoscopeEffect::reset() {
    if (!program) {
        return;
    }
    // TODO
}

const char* KaleidoscopeEffect::getName() {
    return EFFECT_GROUP;
}