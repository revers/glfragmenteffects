/* 
 * File:   KinderpainterEffect.h
 * Author: Revers
 *
 * Created on 20 czerwiec 2012, 07:24
 */

#ifndef KINDERPAINTEREFFECT_H
#define	KINDERPAINTEREFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class KinderpainterEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    KinderpainterEffect(ControlPanel* controlPanel);

    virtual ~KinderpainterEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void mousePressed(int x, int y) {
        mouseAction(x, y);
    }

    virtual void mouseDragged(int x, int y) {
        mouseAction(x, y);
    }
    
    virtual void resize(int x, int y);
    virtual void use();

private:
    void mouseAction(int x, int y);
};

#endif	/* KINDERPAINTEREFFECT_H */

