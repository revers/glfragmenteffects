/* 
 * File:   SevenZeroFourEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 19:15
 */

#ifndef SEVENZEROFOUREFFECT_H
#define	SEVENZEROFOUREFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class SevenZeroFourEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    SevenZeroFourEffect(ControlPanel* controlPanel);

    virtual ~SevenZeroFourEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* SEVENZEROFOUREFFECT_H */

