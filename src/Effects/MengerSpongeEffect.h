/* 
 * File:   MengerSpongeEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 20:17
 */

#ifndef MENGERSPONGEEFFECT_H
#define	MENGERSPONGEEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class MengerSpongeEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    MengerSpongeEffect(ControlPanel* controlPanel);

    virtual ~MengerSpongeEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* MENGERSPONGEEFFECT_H */

