/* 
 * File:   DiscoEffect.h
 * Author: Revers
 *
 * Created on 20 czerwiec 2012, 07:38
 */

#ifndef DISCOEFFECT_H
#define	DISCOEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class DiscoEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    DiscoEffect(ControlPanel* controlPanel);

    virtual ~DiscoEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* DISCOEFFECT_H */

