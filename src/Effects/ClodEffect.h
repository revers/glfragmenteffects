/* 
 * File:   ClodEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 20:37
 */

#ifndef CLODEFFECT_H
#define	CLODEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class ClodEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    ClodEffect(ControlPanel* controlPanel);

    virtual ~ClodEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* CLODEFFECT_H */

