/* 
 * File:   TheRoadOfRibbonEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 19:57
 */

#ifndef THEROADOFRIBBONEFFECT_H
#define	THEROADOFRIBBONEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class TheRoadOfRibbonEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    TheRoadOfRibbonEffect(ControlPanel* controlPanel);

    virtual ~TheRoadOfRibbonEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* THEROADOFRIBBONEFFECT_H */

