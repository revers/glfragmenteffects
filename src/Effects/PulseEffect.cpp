/* 
 * File:   PulseEffect.cpp
 * Author: Revers
 * 
 * Created on 18 czerwiec 2012, 19:24
 */

#include <GL/glew.h>
#include <GL/gl.h>

#include "PulseEffect.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define EFFECT_GROUP "Pulse"
#define EFFECT_PREFIX "pulse_"
#define VERTEX_SHADER_FILE "shaders/Default.vert"
#define FRAGMENT_SHADER_FILE "shaders/Pulse.frag"

Logger PulseEffect::logger = Logger::getInstance("effects.Pulse");

PulseEffect::PulseEffect(ControlPanel* controlPanel)
: AbstractEffect(controlPanel) {
    antiAliasingOn = false;
}

bool PulseEffect::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderFromFile(VERTEX_SHADER_FILE,
            GLSLShader::VERTEX)) {
        LOG_ERROR(logger, "Compilation of file " VERTEX_SHADER_FILE " FAILED!!");
        return false;
    }

    if (!program->compileShaderFromFile(FRAGMENT_SHADER_FILE,
            GLSLShader::FRAGMENT)) {
        LOG_ERROR(logger, "Compilation of file " FRAGMENT_SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking program FAILED!!");
        return false;
    }

    addLoadButton(EFFECT_PREFIX "effect", EFFECT_GROUP);

    int opened = 0;
    TwSetParam(effectBar, EFFECT_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    return true;
}

void PulseEffect::mouseAction(int x, int y) {
    program->use();
    program->setUniform("Mouse", vec2(x, y));
}

void PulseEffect::reset() {
    if (!program) {
        return;
    }

    program->use();
    program->setUniform("Mouse", vec2(0.5f * controlPanel->imageWidth,
            0.5f * controlPanel->imageWidth));
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
}

const char* PulseEffect::getName() {
    return EFFECT_GROUP;
}

void PulseEffect::use() {
    resize(controlPanel->imageWidth, controlPanel->imageHeight);
    controlPanel->setEffect(this);
}

void PulseEffect::resize(int x, int y) {
    program->use();
    program->setUniform("Resolution", glm::vec2(x, y));

    program->setUniform("Mouse", vec2(0.5f * controlPanel->imageWidth,
            0.5f * controlPanel->imageWidth));
}