/* 
 * File:   AbstractEffect.h
 * Author: Revers
 *
 * Created on 16 czerwiec 2012, 21:50
 */

#include <iostream>
#include <string>
#include <string.h>

#include "../ControlPanel.h"
#include "AbstractEffect.h"

using namespace rev;
using namespace std;

AbstractEffect::AbstractEffect(ControlPanel* controlPanel_) : controlPanel(controlPanel_) {
    program = GLSLProgramPtr();
    effectBar = controlPanel->effectBar;
    antiAliasingOn = true;
}

void AbstractEffect::addLoadButton(const char* twName, const char* twGroup) {
    std::string params = "label='Load Filter' group='";
    params += twGroup;
    params += "' ";
    TwAddButton(effectBar, twName, loadButtonCallback,
            this, params.c_str());
}

void AbstractEffect::use() {
    //cout << "AbstractFilter::use()" << endl;
    controlPanel->setEffect(this);
}

void TW_CALL AbstractEffect::loadButtonCallback(void* clientData) {
    AbstractEffect* filter = static_cast<AbstractEffect*> (clientData);
    filter->use();
}

bool AbstractEffect::amILoaded() {
    return strcmp(getName(), controlPanel->getEffect()->getName()) == 0;
}

void AbstractEffect::apply() {
    glActiveTexture(GL_TEXTURE0);
    controlPanel->texture.bind();
    program->use();
}

