/* 
 * File:   SquareTunnelEffect.h
 * Author: Revers
 *
 * Created on 18 czerwiec 2012, 19:12
 */

#ifndef SQUARETUNNELEFFECT_H
#define	SQUARETUNNELEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class SquareTunnelEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    SquareTunnelEffect(ControlPanel* controlPanel);

    virtual ~SquareTunnelEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
};

#endif	/* SQUARETUNNELEFFECT_H */

