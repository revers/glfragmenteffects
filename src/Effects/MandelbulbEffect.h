/* 
 * File:   MandelbulbEffect.h
 * Author: Revers
 *
 * Created on 20 czerwiec 2012, 07:32
 */

#ifndef MANDELBULBEFFECT_H
#define	MANDELBULBEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class MandelbulbEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    MandelbulbEffect(ControlPanel* controlPanel);

    virtual ~MandelbulbEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* MANDELBULBEFFECT_H */

