/* 
 * File:   ReliefTunnelEffect.h
 * Author: Revers
 *
 * Created on 16 czerwiec 2012, 21:46
 */

#ifndef RELIEFTUNNELEFFECT_H
#define	RELIEFTUNNELEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class ReliefTunnelEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    ReliefTunnelEffect(ControlPanel* controlPanel);

    virtual ~ReliefTunnelEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
};

#endif	/* RELIEFTUNNELEFFECT_H */

