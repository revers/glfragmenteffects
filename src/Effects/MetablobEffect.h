/* 
 * File:   MetablobEffect.h
 * Author: Revers
 *
 * Created on 18 czerwiec 2012, 21:24
 */

#ifndef METABLOBEFFECT_H
#define	METABLOBEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class MetablobEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    MetablobEffect(ControlPanel* controlPanel);

    virtual ~MetablobEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
};

#endif	/* METABLOBEFFECT_H */

