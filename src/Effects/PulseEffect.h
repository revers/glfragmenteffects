/* 
 * File:   PulseEffect.h
 * Author: Revers
 *
 * Created on 18 czerwiec 2012, 19:24
 */

#ifndef PULSEEFFECT_H
#define	PULSEEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class PulseEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    PulseEffect(ControlPanel* controlPanel);

    virtual ~PulseEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();

    virtual void mousePressed(int x, int y) {
        mouseAction(x, y);
    }

    virtual void mouseDragged(int x, int y) {
        mouseAction(x, y);
    }

private:
    void mouseAction(int x, int y);
};

#endif	/* PULSEEFFECT_H */

