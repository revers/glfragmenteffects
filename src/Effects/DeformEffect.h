/* 
 * File:   DeformEffect.h
 * Author: Revers
 *
 * Created on 18 czerwiec 2012, 18:50
 */

#ifndef DEFORMEFFECT_H
#define	DEFORMEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class DeformEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    DeformEffect(ControlPanel* controlPanel);

    virtual ~DeformEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void mousePressed(int x, int y) {
        mouseAction(x, y);
    }

    virtual void mouseDragged(int x, int y) {
        mouseAction(x, y);
    }

private:
    void mouseAction(int x, int y);
};

#endif	/* DEFORMEFFECT_H */

