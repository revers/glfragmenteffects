/* 
 * File:   NautilusEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 20:08
 */

#ifndef NAUTILUSEFFECT_H
#define	NAUTILUSEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class NautilusEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    NautilusEffect(ControlPanel* controlPanel);

    virtual ~NautilusEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* NAUTILUSEFFECT_H */

