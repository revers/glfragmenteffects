/* 
 * File:   WaterEffect.h
 * Author: Revers
 *
 * Created on 18 czerwiec 2012, 20:31
 */

#ifndef WATEREFFECT_H
#define	WATEREFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class WaterEffect : public AbstractEffect {
    static log4cplus::Logger logger;

    float intensity;
    int steps;
    float frequency;
    int angle;

    float delta;
    float intence;
    float emboss;
public:
    WaterEffect(ControlPanel* controlPanel);

    virtual ~WaterEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();


private:
    static void TW_CALL setIntensityCallback(const void* value, void* clientData);
    static void TW_CALL getIntensityCallback(void* value, void* clientData);
    
    static void TW_CALL setStepsCallback(const void* value, void* clientData);
    static void TW_CALL getStepsCallback(void* value, void* clientData);
    
    static void TW_CALL setFrequencyCallback(const void* value, void* clientData);
    static void TW_CALL getFrequencyCallback(void* value, void* clientData);
    
    static void TW_CALL setAngleCallback(const void* value, void* clientData);
    static void TW_CALL getAngleCallback(void* value, void* clientData);
    
    static void TW_CALL setDeltaCallback(const void* value, void* clientData);
    static void TW_CALL getDeltaCallback(void* value, void* clientData);
    
    static void TW_CALL setIntenceCallback(const void* value, void* clientData);
    static void TW_CALL getIntenceCallback(void* value, void* clientData);
    
    static void TW_CALL setEmbossCallback(const void* value, void* clientData);
    static void TW_CALL getEmbossCallback(void* value, void* clientData);
};

#endif	/* WATEREFFECT_H */

