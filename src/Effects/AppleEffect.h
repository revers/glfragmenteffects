/* 
 * File:   AppleEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 19:47
 */

#ifndef APPLEEFFECT_H
#define	APPLEEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class AppleEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    AppleEffect(ControlPanel* controlPanel);

    virtual ~AppleEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* APPLEEFFECT_H */

