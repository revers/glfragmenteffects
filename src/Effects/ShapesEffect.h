/* 
 * File:   ShapesEffect.h
 * Author: Revers
 *
 * Created on 18 czerwiec 2012, 21:07
 */

#ifndef SHAPESEFFECT_H
#define	SHAPESEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class ShapesEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    ShapesEffect(ControlPanel* controlPanel);

    virtual ~ShapesEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
};

#endif	/* SHAPESEFFECT_H */

