/* 
 * File:   PlasmaEffect.h
 * Author: Revers
 *
 * Created on 20 czerwiec 2012, 07:46
 */

#ifndef PLASMAEFFECT_H
#define	PLASMAEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class PlasmaEffect : public AbstractEffect {
    static log4cplus::Logger logger;
    float factor;
public:
    PlasmaEffect(ControlPanel* controlPanel);

    virtual ~PlasmaEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
    
private:
    static void TW_CALL setFactorCallback(const void* value, void* clientData);
    static void TW_CALL getFactorCallback(void* value, void* clientData);
};

#endif	/* PLASMAEFFECT_H */

