/* 
 * File:   DroidEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 22:03
 */

#ifndef DROIDEFFECT_H
#define	DROIDEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class DroidEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    DroidEffect(ControlPanel* controlPanel);

    virtual ~DroidEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* DROIDEFFECT_H */

