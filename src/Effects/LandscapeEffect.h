/* 
 * File:   LandscapeEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 19:37
 */

#ifndef LANDSCAPEEFFECT_H
#define	LANDSCAPEEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class LandscapeEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    LandscapeEffect(ControlPanel* controlPanel);

    virtual ~LandscapeEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* LANDSCAPEEFFECT_H */

