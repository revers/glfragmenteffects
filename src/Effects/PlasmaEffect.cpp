/* 
 * File:   PlasmaEffect.cpp
 * Author: Revers
 * 
 * Created on 20 czerwiec 2012, 07:46
 */

#include <GL/glew.h>
#include <GL/gl.h>

#include "PlasmaEffect.h"
#include "../ControlPanel.h"

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

using namespace rev;
using namespace log4cplus;
using namespace std;

#define EFFECT_GROUP "Plasma"
#define EFFECT_PREFIX "Plasma_"
#define VERTEX_SHADER_FILE "shaders/Default.vert"
#define FRAGMENT_SHADER_FILE "shaders/Plasma.frag"

Logger PlasmaEffect::logger = Logger::getInstance("effects.Plasma");

#define UNIFORM_FACTOR "Factor"
#define DEFAULT_FACTOR 500.0f

PlasmaEffect::PlasmaEffect(ControlPanel* controlPanel)
: AbstractEffect(controlPanel) {
    //antiAliasingOn = false;
    factor = DEFAULT_FACTOR;
}

bool PlasmaEffect::init() {
    LOG_TRACE(logger, "init()");

    program = GLSLProgramPtr(new GLSLProgram);

    if (!program->compileShaderFromFile(VERTEX_SHADER_FILE,
            GLSLShader::VERTEX)) {
        LOG_ERROR(logger, "Compilation of file " VERTEX_SHADER_FILE " FAILED!!");
        return false;
    }

    if (!program->compileShaderFromFile(FRAGMENT_SHADER_FILE,
            GLSLShader::FRAGMENT)) {
        LOG_ERROR(logger, "Compilation of file " FRAGMENT_SHADER_FILE " FAILED!!");
        return false;
    }

    glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
    glBindAttribLocation(program->getHandle(), 1, "VertexTexCoord");
    glBindAttribLocation(program->getHandle(), 0, "FragColor");

    if (!program->link()) {
        LOG_ERROR(logger, "Linking program FAILED!!");
        return false;
    }

    TwAddVarCB(effectBar, EFFECT_PREFIX UNIFORM_FACTOR, TW_TYPE_FLOAT,
            setFactorCallback, getFactorCallback, this,
            "label='" UNIFORM_FACTOR ":' min=0.0 max=1000.0 step=1.0 group='"
            EFFECT_GROUP "' ");

    addLoadButton(EFFECT_PREFIX "effect", EFFECT_GROUP);

    int opened = 0;
    TwSetParam(effectBar, EFFECT_GROUP, "opened", TW_PARAM_INT32, 1, &opened);

    program->use();
    program->setUniform(UNIFORM_FACTOR, factor);

    return true;
}

void PlasmaEffect::reset() {
    if (!program) {

        return;
    }

    factor = DEFAULT_FACTOR;

    program->use();
    program->setUniform(UNIFORM_FACTOR, factor);
}

const char* PlasmaEffect::getName() {

    return EFFECT_GROUP;
}

void TW_CALL
PlasmaEffect::setFactorCallback(const void* value, void* clientData) {
    PlasmaEffect* effect = static_cast<PlasmaEffect*> (clientData);
    effect->factor = *(const float*) value;

    if (!effect->program || !effect->amILoaded()) {

        return;
    }

    effect->program->setUniform(UNIFORM_FACTOR, effect->factor);
}

void TW_CALL
PlasmaEffect::getFactorCallback(void* value, void* clientData) {
    PlasmaEffect* effect = static_cast<PlasmaEffect*> (clientData);

    *(float*) value = effect->factor;
}