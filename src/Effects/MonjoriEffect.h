/* 
 * File:   MonjoriEffect.h
 * Author: Revers
 *
 * Created on 18 czerwiec 2012, 07:37
 */

#ifndef MONJORIEFFECT_H
#define	MONJORIEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class MonjoriEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    MonjoriEffect(ControlPanel* controlPanel);

    virtual ~MonjoriEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();
};

#endif	/* MONJORIEFFECT_H */

