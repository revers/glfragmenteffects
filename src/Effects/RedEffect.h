/* 
 * File:   RedEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 21:12
 */

#ifndef REDEFFECT_H
#define	REDEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class RedEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    RedEffect(ControlPanel* controlPanel);

    virtual ~RedEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};
#endif	/* REDEFFECT_H */

