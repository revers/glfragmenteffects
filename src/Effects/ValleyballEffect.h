/* 
 * File:   ValleyballEffect.h
 * Author: Revers
 *
 * Created on 19 czerwiec 2012, 21:00
 */

#ifndef ValleyballEFFECT_H
#define	ValleyballEFFECT_H

#include "AbstractEffect.h"

namespace log4cplus {
    class Logger;
};

class ValleyballEffect : public AbstractEffect {
    static log4cplus::Logger logger;
public:
    ValleyballEffect(ControlPanel* controlPanel);

    virtual ~ValleyballEffect() {
    }

    virtual bool init();
    virtual void reset();
    virtual const char* getName();

    virtual void resize(int x, int y);

    virtual void use();
};

#endif	/* ValleyballEFFECT_H */

